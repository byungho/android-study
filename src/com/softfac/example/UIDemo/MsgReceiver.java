package com.softfac.example.UIDemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: byungho
 * Date: 13. 2. 22
 * Time: 오후 3:06
 * To change this template use File | Settings | File Templates.
 */
public class MsgReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Log.e("UIDemo", "MsgReceiver -> Begin");
        try
        {
            if (null == intent)
            {
                Log.e("UIDemo", "MsgReceiver -> intent is NULL");
            }
            else
            {
                int idx = 0;
                String s;
                do {
                    String key = String.format("Notify%d", idx++);
                    s = intent.getStringExtra(key);
                    if (0 < s.length())
                    {
                        Log.e("UIDemo", "MsgReceiver -> " + s);


                    }
                    else
                        Log.e("UIDemo", "MsgReceiver -> No data");
                }
                while (0 < s.length());
            }
        } catch (Exception localException) {
            localException.printStackTrace();
        }

        Log.e("UIDemo", "MsgReceiver -> End.");

    }
}
