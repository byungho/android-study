package com.softfac.example.UIDemo;

import android.os.AsyncTask;

/**
 * Created with IntelliJ IDEA.
 * User: guyoun
 * Date: 2/14/13
 * Time: 5:44 PM
 * To change this template use File | Settings | File Templates.
 */
public interface AsyncTaskResponse<Progress, Result> {
    public void taskStart();
    public void taskProgress(Progress progress);
    public void taskComplete(Result result);
}
