package com.softfac.example.UIDemo;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created with IntelliJ IDEA.
 * User: guyoun
 * Date: 2/14/13
 * Time: 3:17 PM
 * To change this template use File | Settings | File Templates.
 */
class DownloadTask extends AsyncTask<String, Integer, byte[]>{
    protected AsyncTaskResponse mListener = null;

    protected void setListener(AsyncTaskResponse listener){
        mListener = listener;
    }

    @Override
    protected byte[] doInBackground(String... urls) {
        HttpURLConnection urlConnection = null;
        byte[] result = null;

        try {
            URL url = new URL(urls[0]);
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.connect();

            InputStream input = urlConnection.getInputStream();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            byte data[] = new byte[1]; //1024];
            long fileLength = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                fileLength += count;
                bos.write(data, 0, count);
                if (isCancelled()) break;
                publishProgress((int)fileLength);
            }
            result = bos.toByteArray();
            result = Base64.decode(result, Base64.DEFAULT);

            Log.d("DownloadTask", "fileLength: " + Long.toString(fileLength));
            Log.d("DownloadTask", "download byte array: " + bos.toString());
            Log.d("DownloadTask", "download byte array: " + new String(result));

            bos.close();
            input.close();
        } catch (Exception e) {
            Log.e("DownloadTask", e.toString());
            e.printStackTrace();
        }
        finally {
            if(urlConnection != null)
                urlConnection.disconnect();
        }

        return result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Log.v("DownloadTask", "onPreExecute");
        if(mListener != null)
            mListener.taskStart();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);

        Log.v("DownloadTask", "onProgressUpdate");
        if(mListener != null)
            mListener.taskProgress(progress[0]);
    }

    @Override
    protected void onPostExecute(byte[] result){
        super.onPostExecute(result);

        Log.v("DownloadTask", "onPostExecute");
        if(mListener != null)
            mListener.taskComplete(result);
    }
}