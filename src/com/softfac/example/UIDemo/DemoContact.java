package com.softfac.example.UIDemo;

import android.content.Context;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: byungho
 * Date: 13. 2. 1
 * Time: 오후 4:12
 * To change this template use File | Settings | File Templates.
 */
public class DemoContact implements Serializable{
    private static final long serialVersionUID = -4589885080580317958L;

    private String mName;
    private String mPhoneNumber;
    private String mDescription;

    public DemoContact(Context context, String aName, String aPhoneNumber, String aDesc) {
        mName = aName;
        mPhoneNumber = aPhoneNumber;
        mDescription = aDesc;
    }

    public String getName() {
        return mName;
    }
    public String getPhoneNumber() {
        return mPhoneNumber;
    }
    public String getDescription() {
        return mDescription;
    }

    public void setName(String aName) {
        mName = aName;
    }
    public void setPhonenumber(String aPhoneNumber) {
        mPhoneNumber = aPhoneNumber;
    }
    public void setDescription(String aDesc) {
        mDescription = aDesc;
    }
}
