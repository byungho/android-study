package com.softfac.example.UIDemo;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: guyoun
 * Date: 2/14/13
 * Time: 3:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class UploadTask extends AsyncTask<String, Integer, Integer> {
    protected HttpPost mHttpPost;
    protected AsyncTaskResponse mListener = null;

    protected void setListener(AsyncTaskResponse listener){
        mListener = listener;
    }

    @Override
    protected Integer doInBackground(String... params) {
        Integer status_code = null;
        try {
            status_code = upload(params[0], params[1]);
        } catch(Exception e){
            Log.e("UploadTask", "doInBackground exception");
            Log.e("UploadTask", e.getMessage());
        }

        return status_code;
    }

    private void setEntity(String data){
        try{
            List nameValuePairs = new ArrayList();
            data = Base64.encodeToString(data.getBytes(), Base64.DEFAULT);
            nameValuePairs.add(new BasicNameValuePair("data", data));
            mHttpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            Log.d("UploadTask", mHttpPost.getRequestLine().toString());
        }catch(Exception e){
            Log.e("UploadTask", "setEntity exception");
        }

    }

    private Integer upload(String URL, String filename){
        Integer status_code = null;
        HttpResponse response = null;
        mHttpPost = new HttpPost(URL);
        Log.d("UploadTask", "Upload URL" + URL);

        setEntity(filename);
        HttpClient httpclient = new DefaultHttpClient();
        try{
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            response = httpclient.execute(mHttpPost);
            status_code = new Integer(response.getStatusLine().getStatusCode());
        }catch(Exception e){
            Log.v("UploadTask", e.toString());
        }

        return status_code;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.v("UploadTask", "onPreExecute");

        if(mListener != null)
            mListener.taskStart();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        Log.v("UploadTask", "onProgressUpdate");

        if(mListener != null)
            mListener.taskProgress(progress);
    }

    @Override
    protected void onPostExecute(Integer result){
        super.onPostExecute(result);

        Log.v("UploadTask", "onPostExecute");

        if(mListener != null)
            mListener.taskComplete(result);
    }
}