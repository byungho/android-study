package com.softfac.example.UIDemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.*;
import android.widget.*;
import android.util.Log;
import android.content.Context;

import java.io.*;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class UIDemoMyActivity extends Activity{

    final static int CONTACT_EDIT = 1;
    final static int CONTACT_ADD = 2;

    private static final int ENTRY_MODIFY = Menu.FIRST;
    private static final int ENTRY_REMOVE = Menu.FIRST + 1;

    ListView lvItems;

    // Custom
    ArrayList<DemoContact> mContactItems = null;
    DemoContactAdapter mContactAdapter;

    int iEditPosition;

    ProgressDialog mProgressDialog;
    DownloadTask mDownloadTask;
    UploadTask mUploadTask;

    Intent mMsgPullServiceIntent;
    MsgReceiver mReceiver;
    Boolean MsgReceiverRegisted;
    Handler mHandler;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // Regist Receiver
        mReceiver = new MsgReceiver();
        registerReceiver(mReceiver, new IntentFilter("NotifyBroadcast"));
        MsgReceiverRegisted = true;

        //ProgressDialog
        mProgressDialog = new ProgressDialog(UIDemoMyActivity.this);
        mProgressDialog.setMessage("Downloading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(512);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        if(isConnected())
        {
            Toast toast = Toast.makeText(UIDemoMyActivity.this, "Network is connected.",Toast.LENGTH_SHORT);
            toast.show();
        }

        iEditPosition = -1;
//        doDownload();

        Log.e("UIDemo", "OnCreate");
        if(mContactItems == null){
            Log.d("UIDemo", "mContactItems is null");
            mContactItems = new ArrayList<DemoContact>();
            mContactItems.add(new DemoContact(getApplicationContext(), "Byunho Jeon", "010-5655-0011", "bastad@softfac.co.kr"));

            mContactAdapter = new DemoContactAdapter(this, mContactItems);
        }


        lvItems = (ListView) findViewById(R.id.listView);
        lvItems.setAdapter(mContactAdapter);
        lvItems.setCacheColorHint(0);
        lvItems.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            public void onCreateContextMenu(ContextMenu menu, View v,
                                            ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(0, ENTRY_MODIFY, 1, "Modify");
                menu.add(0, ENTRY_REMOVE, 2, "Remove");

                if(v == (ListView) findViewById(R.id.listView))
                {
                    iEditPosition = ((AdapterView.AdapterContextMenuInfo)menuInfo).position;
                    Log.e("UIDemo", "ListView.OnItemClick() -> " + String.format("Position=%d", iEditPosition));
                }
        }


        });

        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //To change body of implemented methods use File | Settings | File Templates.
                Log.e("UIDemo", "ListView.OnItemClick() -> " + String.format("Position=%d", i));

                DemoContact contact = mContactItems.get(i);
                if (null == contact) Log.e("UIDemo", "ListView.OnItemClick() -> contact is null.");

                Log.e("UIDemo", "ListView.OnItemClick() - Event");

                if (null != contact) {
                    iEditPosition = i;

                    Intent itContact = new Intent(UIDemoMyActivity.this, UIDemoContractActivity.class);
                    itContact.putExtra("name", contact.getName());
                    itContact.putExtra("phonenumber", contact.getPhoneNumber());
                    itContact.putExtra("desc", contact.getDescription());

                    //startActivity(itContact);
                    startActivityForResult(itContact, CONTACT_EDIT);

                    Log.e("UIDemo", "ListView.OnItemClick() - Event -> Started Contact Activity");
                }
            }
        });

        Button button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //To change body of implemented methods use File | Settings | File Templates.
                Log.e("UIDemo", "Button.OnClick");

                Intent itContact = new Intent(UIDemoMyActivity.this, UIDemoContractActivity.class);
                startActivityForResult(itContact, CONTACT_ADD);
            }
        });


        // Run background service
        mMsgPullServiceIntent = new Intent(this, MsgPullService.class);
        startService(mMsgPullServiceIntent);
        Log.e("UIDemo", "StartService - MsgPullService");

        //Timer tm = new Timer();
        //LaunchTask task = new LaunchTask();
        //tm.schedule(task, 1000, 10000); // 10 sec
    }

    @Override
    public void onStop(){
        super.onStop();
        Log.v("UIDemo", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(MsgReceiverRegisted)
        {
            unregisterReceiver(mReceiver);
            MsgReceiverRegisted = false;
            Log.e("UIDemo", "UnregistReceiver");
        }

        mMsgPullServiceIntent = new Intent(this, MsgPullService.class);
        stopService(mMsgPullServiceIntent);
        Log.e("UIDemo", "StopService - MsgPullService");


        //doUpload();
        Log.v("UIDemo", "onDestroy");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Log.e("UIDemo", "Selected item " + info.position);

        switch(item.getItemId()) {
            case ENTRY_MODIFY:
                Log.e("UIDemo", "Modify was clicked");
                DemoContact contact = mContactItems.get(iEditPosition);
                Intent itContact = new Intent(UIDemoMyActivity.this, UIDemoContractActivity.class);
                itContact.putExtra("name", contact.getName());
                itContact.putExtra("phonenumber", contact.getPhoneNumber());
                itContact.putExtra("desc", contact.getDescription());

                //startActivity(itContact);
                startActivityForResult(itContact, CONTACT_EDIT);

                break;
            case ENTRY_REMOVE:
                Log.e("UIDemo", "Remove was clicked");
                Log.e("UIDemo", "ListView.OnItemClick() -> " + String.format("Position=%d", iEditPosition));

                AlertDialog.Builder alert_confirm = new AlertDialog.Builder(UIDemoMyActivity.this);
                alert_confirm.setMessage("Are you sure you want to delete this?").setCancelable(false).setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mContactItems.remove(iEditPosition);
                                mContactAdapter.notifyDataSetChanged();

//                                DemoContact remove = mContactItems.get(iEditPosition);
//                                mContactAdapter.remove(remove);

                                Toast toast = Toast.makeText(UIDemoMyActivity.this, "Have been deleted",Toast.LENGTH_SHORT);
                                toast.show();

                                // 'YES'
                            }
                        }).setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Log.e("UIDemo", "dialog_result " + check);
                                // 'No'
                                return;
                            }
                        });
                AlertDialog alert = alert_confirm.create();
                alert.show();

                break;
            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("UIDemo", "onActivityResult() - " + String.format("req=%d, res=%d", requestCode, resultCode));

        switch (requestCode) {
            case CONTACT_EDIT:
                if (RESULT_OK == resultCode) {
                    Log.e("UIDemo", "onActivityResult() -> OK");
                    //Log.e("UIDemo", String.format("name=%s", data.getStringExtra("name")));

                    if (null == data) Log.e("UIDemo", "onAcitivityResult() -> OK. but Intent is null.");

                    if ((null != data) && (-1 < iEditPosition)) {
                        //mListItems.set(iEditPosition, data.getStringExtra("name"));

                        DemoContact contact = mContactItems.get(iEditPosition);
                        if (null != contact) {
                            contact.setName(data.getStringExtra("name"));
                            contact.setPhonenumber(data.getStringExtra("phonenumber"));
                            contact.setDescription(data.getStringExtra("desc"));

                            Log.e("UIDemo", "Contact data updated. " + String.format("name=%s, phonenumber=%s, desc=%s",
                                    contact.getName(), contact.getPhoneNumber(), contact.getDescription()));

                            lvItems.invalidateViews();

                            Toast toast = Toast.makeText(this, "Have been modifyed",Toast.LENGTH_SHORT);
                            toast.show();
                        }

                        // TODO: Invalidate?
                    }

                } else {
                    Log.e("UIDemo", "onActivityResult() -> FAIL to edit name from ContactActivity");
                }
            break;
            case CONTACT_ADD:
                if (RESULT_OK == resultCode) {
                    Log.e("UIDemo", "onActivityResult() -> OK");
                    //Log.e("UIDemo", String.format("name=%s", data.getStringExtra("name")));

                    if (null == data) Log.e("UIDemo", "onAcitivityResult() -> OK. but Intent is null.");

                    if ((null != data)) {
                        //mListItems.set(iEditPosition, data.getStringExtra("name"));
                        mContactItems.add(new DemoContact(getApplicationContext(), data.getStringExtra("name"), data.getStringExtra("phonenumber"), data.getStringExtra("desc")));
                        mContactAdapter.notifyDataSetChanged();

                        Log.e("UIDemo", "Contact data created.");
                        Toast toast = Toast.makeText(this, "Have been created",Toast.LENGTH_SHORT);
                        toast.show();


                        // TODO: Invalidate?
                    }

                } else {
                    Log.e("UIDemo", "onActivityResult() -> FAIL to edit name from ContactActivity");
                }
            break;
        }

    }

    //network 연결 체크
    public boolean isConnected(){

        ConnectivityManager connMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            // fetch data
            Log.e("Http", "network is connected");
            return true;
        } else {
            Log.e("Http", "network is not connected");
            return false;
        }
    }

    //local file에서 데이타 로드
    protected void loadDataFromFile(){
        String filename = "data";

        try {
            Log.v("UIDemo", "loadDataFromFile");

            byte buff[] = readFile(filename);
            ByteArrayInputStream isb = new ByteArrayInputStream(buff);
            ObjectInputStream iso = new ObjectInputStream(isb);

            Object obj = iso.readObject();
            mContactItems.clear();
            mContactItems.addAll((ArrayList<DemoContact>)obj);
            mContactAdapter.notifyDataSetChanged();

//            mContactAdapter.clear();
//            mContactAdapter.addAll(mContactItems);
//            mContactAdapter = new DemoContactAdapter(this, mContactItems);

            iso.close();
            isb.close();
        }
        catch(Exception e){
            Log.e("UIDemo", "loadDataFromFile exception");
            e.printStackTrace();
        }
    }

    protected byte[] readDataFromListView(){
        byte[] result = null;

        ByteArrayOutputStream osByteArray;
        ObjectOutputStream osObject;

        try {
            osByteArray = new ByteArrayOutputStream();
            osObject = new ObjectOutputStream(osByteArray);

            osObject.writeObject(mContactItems);
            result = osByteArray.toByteArray();

            Log.d("UIDemo", "readDataFromListView : " + osByteArray.toString());
            Log.d("UIDemo", Integer.toString(osByteArray.size()));

            osObject.close();
            osByteArray.close();
        }
        catch(Exception e){
            Log.e("UIDemo", "readDataFromListView exception");
            Log.e("UIDemo", e.toString());
        }

        return result;
    }

    protected void writeDataToFile(){
       String filename = "data";
      try {
        byte[] data = readDataFromListView();
        writeFile(filename, data);
      }
      catch(Exception e){
        Log.e("UIDemo", "writeDataToFile exception");
        Log.e("UIDemo", e.toString());
      }
    }

    protected byte[] readFile(String filename){
        FileInputStream isFile;
        ByteArrayOutputStream osb = null;
        byte[] result = null;

        try {
            isFile = openFileInput(filename);
            osb = new ByteArrayOutputStream();
            byte data[] = new byte[1024];
            int count;
            while ((count = isFile.read(data)) != -1) {
                osb.write(data,0, count);
            }
            result = osb.toByteArray();

            if(osb != null)
                osb.close();

            isFile.close();
        }
        catch(Exception e){
            Log.e("UIDemo", "readFile exception");
            Log.e("UIDemo", e.toString());
        }
        return result;
    }

    protected void writeFile(String filename, byte[] data) throws Exception{
        FileOutputStream osFile = null;

        try {
            osFile = openFileOutput(filename, Context.MODE_PRIVATE);
            osFile.write(data);
            osFile.flush();

            Log.v("UIDemo", "writeFile");
        }
        catch(Exception e){
            Log.e("UIDemo", "writeFile exception");
            Log.e("UIDemo", e.toString());
        }finally{
            if(osFile != null)
                osFile.close();

        }
    }

    //UploadTask 실행 및 결과 처리
    public void doUpload(){
        this.writeDataToFile();

        UploadTask mUploadTask = new UploadTask();
        mUploadTask.setListener(new AsyncTaskResponse<Integer, Integer>() {
            public void taskStart() {
                //To change body of implemented methods use File | Settings | File Templates.
                Log.d("UIDemo", "UploadTask start");
            }

            @Override
            public void taskProgress(Integer progress) {
                //To change body of implemented methods use File | Settings | File Templates.
                Log.d("UIDemo", "UploadTask progress");
            }

            @Override
            public void taskComplete(Integer result) {
                //To change body of implemented methods use File | Settings | File Templates.
                Log.v("UIDemo", "UploadTask complete");

                if(result.intValue() == 200){
                    Log.d("UIDemo", "UploadTask is completed");
                }else{
                    Log.d("UIDemo", "UploadTask is not completed");
                }



            }

        });
        String urls[] = {
                new String("http://demo.softfac.com/circle/upload.php"),
                new String(this.readDataFromListView())
        };

        mUploadTask.execute(urls);
    }

    //DownloadTask 실행 및 결과 처리
    public void doDownload(){
        mDownloadTask = new DownloadTask();
        mDownloadTask.setListener(new AsyncTaskResponse<Integer, byte[]>() {
            public void taskStart() {
                //To change body of implemented methods use File | Settings | File Templates.
                Log.d("UIDemo", "DownloadTask start");

                mProgressDialog.setProgress(0);
                mProgressDialog.show();
            }

            @Override
            public void taskProgress(Integer progress) {
                //To change body of implemented methods use File | Settings | File Templates.
                Log.d("UIDemo", "DownloadTask progress");

                mProgressDialog.setProgress(progress.intValue());
            }

            @Override
            public void taskComplete(byte[] result) {
                //To change body of implemented methods use File | Settings | File Templates.
                Log.d("UIDemo", "DownloadTask is completed");

                mProgressDialog.dismiss();

                Log.d("UIDemo", "Download: " + new String(result));

                try{
                    if(!result.equals("null")){
                        writeFile("download", result);
                        loadDataFromFile();
                    }
                }catch(Exception e){
                    Log.e("UIDemo", "DownloadComplete exception");
                }

            }
        });

        String urls[] = {
                new String("http://demo.softfac.com/circle/download.php")
        };
        mDownloadTask.execute(urls);
    }

    //업로드 버튼 이벤트 핸들러
    public void OnUploadClick(View view){
        doUpload();
    }

    //다운로드 버튼 이벤트 핸들러
    public void OnDownloadClick(View view){
        //downalod data
        doDownload();
    }

    //체크 버튼 이벤트 핸들러
    //다운로드 및 로컬 파일 비교
    public void onCheckClick(View view){
        try {
            byte[] data = readDataFromListView();
            writeFile("data", data);
            //writeFile("download", data);

            byte[] file1 = readFile("download");
            byte[] file2 = readFile("data");
            String data_str = new String(data);
            String file1_str = new String(file1);
            String file2_str = new String(file2);

            loadDataFromFile();
            Log.d("UIDemo", "memory : " + data_str);
            Log.d("UIDemo", "file1 : " + file1_str);
            Log.d("UIDemo", "file2 : " + file2_str);

            if(data_str.equals(file1_str))
                Log.d("UIDemo", "data == file1_str");

            if(data_str.equals(file2_str))
                Log.d("UIDemo", "data == file2_str");
        }
        catch(Exception e){
            Log.e("UIDemo", "onCheckClick exception");
            Log.e("UIDemo", e.toString());
        }
    }

    /*
    class LaunchTask extends TimerTask {
        public void run() {
            Log.e("UIDemo", "LaunchTask -> Begin");

            //mMsgPullServiceIntent = new Intent(UIDemoMyActivity.this, MsgPullService.class);
            //startService(mMsgPullServiceIntent);
            //Log.e("UIDemo", "LaunchTask -> Start MsgPullService");

        }
    }
    */

}

