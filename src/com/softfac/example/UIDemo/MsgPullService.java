package com.softfac.example.UIDemo;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import org.apache.http.HttpStatus;

import java.io.IOException;
import java.io.InputStream;
import java.lang.String;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;



/**
 * Created with IntelliJ IDEA.
 * User: byungho
 * Date: 13. 2. 22
 * Time: 오전 11:06
 * To change this template use File | Settings | File Templates.
 */
public class MsgPullService extends IntentService {

    Intent mItNotify = new Intent("NotifyBroadcast");

    public MsgPullService(){
        super("MsgPullService");
    }


    @Override
    protected void onHandleIntent(Intent workIntent) {
        Log.e("MsgPullService", "MSGPullService -> HandleIntent: Begin");


        if (null == workIntent)
            Log.e("MsgPullService", "Null WorkIntent");
        else
        {
            String dataString = workIntent.getDataString();
            Log.e("MsgPullService","dataString=" + dataString);
        }


        //String sURL = workIntent.getStringExtra("URL");
        String sURL = "http://release.softfac.com/android_msg_pull.php";
        Log.e("MsgPullService", "URL=" + sURL);

        java.net.URL localURL;

        try {
            localURL = new URL(sURL);

            // Tries to open a connection to the URL. If an IO error occurs, this throws an IOException
            URLConnection localURLConnection = localURL.openConnection();

            Log.e("MsgPullService", "url connection ");

            // If the connection is an HTTP connection, continue
            if ((localURLConnection instanceof HttpURLConnection)) {

                // Broadcasts an Intent indicating that processing has started.
                //mBroadcaster.broadcastIntentWithState(Constants.STATE_ACTION_STARTED);
                Log.e("MsgPullService", "started!");

                // Casts the connection to a HTTP connection
                HttpURLConnection localHttpURLConnection = (HttpURLConnection) localURLConnection;

                // Sets the user agent for this request.
                localHttpURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; U; Android; UIDemo)");

                // request
                int responseCode = localHttpURLConnection.getResponseCode();

                if (HttpStatus.SC_OK == responseCode) {
                    Log.e("MsgPullService", "Http -> OK ");

                    // Gets the last modified data for the URL
                    //long lastModifiedDate = localHttpURLConnection.getLastModified();

                    String sResponse = "";

                    InputStream text = localURLConnection.getInputStream();
                    byte[] bytes = new byte[4096];
                    int iReaded, iTotal;
                    iTotal = 0;
                    do
                    {
                        iReaded = text.read(bytes);
                        if (-1 == iReaded) break;
                        Log.e("MsgPullService", "Readed=" + new Integer(iReaded).toString());
                        if (0 < iReaded)
                        {
                                iTotal += iReaded;
                                String s = new String(bytes, 0, iReaded);
                                sResponse += s; //new String(s.getBytes(), "UTF-8");
                                //sResponse = sResponse.concat(s.getBytes("UTF-8"));
                        }
                    }
                    while (true);

                    Log.e("MsgPullService", "Total=" + new Integer(iTotal).toString());
                    Log.e("MsgPullService", sResponse);

                    int idx = 0;
                    String list[] = sResponse.split("\n");
                    for (int i = 0; i < list.length; i++)
                    {
                        if (0 < list[i].length())
                        {
                            Log.e("MsgPullService", String.format("%d => %s", i, list[i]));
                            mItNotify.putExtra(String.format("Notify%d", idx++), list[i]);
                        }
                    }

                    if(0 < idx) {
                        //LocalBroadcastManager.getInstace(this).sendBroadcast(itNotify);
                        sendBroadcast(mItNotify);
                        Log.e("MsgPullService", "sendBroadcast");
                    }


                }
                else
                {
                    Log.e("MsgPullService", "Http -> Fail: ");
                }
                localHttpURLConnection.disconnect();

            }
            else
            {
                Log.e("MsgPullService", "None of HttpURLConnection");
            }

            //this.wait(2000);

            // Handles possible exceptions
        } catch (MalformedURLException localMalformedURLException) {
            Log.e("MsgPullService", "Except(1)");
            localMalformedURLException.printStackTrace();

        } catch (IOException localIOException) {
            Log.e("MsgPullService", "Except(2)");
            localIOException.printStackTrace();

        } finally {
            Log.e("MsgPullService", "MSGPullService -> HandleIntent: Loop END.");
        }

        Log.e("MsgPullService", "MSGPullService -> HandleIntent: End");
    }
}
